# NODEJS: TEST-	DRIVEN DEVELOPMENT

- Concepts we're building on: Javascript, node.js, koa, generators

## 1. Understanding test-driven development
- tdd >< bdd (behavior-drive development)
- What is testing and test first: traditional teting: plan and write code -> test; test first: write test -> fail test -> code to pass test

## 2. Testing your data layer 
- [mocha](https://mochajs.org/): Javascript test framework running on Node.js and the browser; mocha work on test directory; install globally to use keyword `mocha` in command line
	- run test in folder test
	- `mocha` (global) or `./node_modules/mocha/bin/mocha` (local)
	- `describe(str, func)`: describe the feature, can nest describe, put `it` function in *func*
	- `it(str, func)`: put expectation in *func*, pass when true to all expectation in *func*. pass -> color green (in cmd), fail -> color red. *func* can receive arg `done` to invoke a callback when the test is complete; `done` can accept arg `err`
	- passing arrow functions to Mocha is discourage
	- `this.timeout()`: apply to entire test suites; disabled via `this.timeout(0)`
	- `before(func)`: runs before all tests in block
	- `after(func)`, `beforeEach(func)`, `afterEach(func)`
- `mocha --harmony`
- Make sure you fail first: `npm install co-mocha`
- Getting your test to pass: create user-data.js, `require('co-mocha')` in test file
	- user-date.js: exports users: {get: function*(){}, save: function*(){}}`
	- user-api-spec.js: `var should = require('should')`; `require('co-mocha')`, `require('co-fs')`

## 3. Testing your web layer
- [supertest](https://www.npmjs.com/package/supertest): http assertions, install `--save-dev`
	- `var request = require('supertest');`
	- `request(app).get("/").expect(200).end(done);`
	- `request(app).post("/dictionary-api").send({ "term": "Three", "defined": "Term Three Defined"}).expect(200).end(done);`
	- when running request, console.log `<method> request for '<url>' - <body>` such as `GET request for '/' - {}`
 
- `npm install co-supertest`, `npm install koa`, `npm install koa-router` -> integration test
- code refactoring: make code more readable, change code without change functionality; run test to test if changed functions
- Trying the final product: `node --harmony user-web`